
public class Comparer {
	
	/*
	 * Write a java method that takes two integers, num1 and num2
	 * as its parameters, and returns an integer.
	 * It should return the larger of the two numbers.
	 * 
	 * For all the smarty pants who finished this:
	 * Write a java method that takes three integers,
	 * num1, num2, num3
	 * It should return the largest of the three numbers.
	 * */
	
	public static int largest_A(int num1, int num2, int num3) {
		int largest = 0;
		if (num1 > num2) {
			if (num2 > num3) {
				largest = num1;
			}
			else {
				largest = larger(num1, num3);
			}
		}
		else {
			if (num1 > num3) {
				largest = num2;
			}
			else {
				largest = larger(num2, num3);
			}
		}
		return largest;
	}
	
	public static int largest_B(int num1, int num2, int num3) {
		if (num1 > num2 && num1 > num3)
			return num1;
		if (num2 > num1 && num2 > num3)
			return num2;
		return num3;
	}
	
	public static int largest_C(int num1, int num2, int num3) {
		int currentLargest = num1;
		if (num2 > currentLargest)
			currentLargest = num2;
		if (num3 > currentLargest)
			currentLargest = num3;
		return currentLargest;
	}
	
	public static int isEven(int num) {
		if (num % 2 == 0) 
			return 1;
		return 0;
	}
	
	public static int larger(int num1, int num2) {
		if (num1 > num2)
			return num1;
		return num2;
	}
	
	public static void main(String[] args) {
		System.out.println(largest_B(52, 25, 30));
	}

}
