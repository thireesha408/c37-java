
public class Day19 {
	
	public static int[] remove (int[] arr, int index) {
		int[] temp = new int [arr.length - 1];
		int j = 0;
		for (int i = 0; i < arr.length; i++) {
			if (i != index)
				temp[j++] = arr[i];
		}
		return temp;
	}
	public static int[] removeDuplicates (int[] arr) {
		int i = 0;
		while (i < arr.length) {
			int j = i + 1;
			while (j < arr.length) {
				if (arr[j] == arr[i])
					arr = remove(arr, j);
				else
					j++;
			}
			i++;
		}
		return arr;
	}
	public static int sum (int[] arr) {
		int sum = 0;
		for (int elem : arr)
			sum += elem;
		return sum;
	}
	
	public static void main (String[] args) {
		int[] a = {1, 2, 3, 4, 12};
		System.out.println(sum(removeDuplicates(a)));
	}

}
