
public class Sorter {
	
	public static void insertionSort (int[] arr) {
		int insIndex = 0;
		for (int sortedUntil = 1; sortedUntil < arr.length; sortedUntil++) {
			int elementToPlace = arr[sortedUntil];
			for (insIndex = sortedUntil; insIndex > 0 && arr[insIndex - 1] > elementToPlace; insIndex--)
				arr[insIndex] = arr[insIndex - 1];
			arr[insIndex] = elementToPlace;
		}
	}
	
	public static int minIndexFrom (int[] arr, int indexFrom) {
		int minIndex = indexFrom;
		for (int i = indexFrom; i < arr.length; i++)
			if (arr[i] < arr[minIndex])
				minIndex = i;
		return minIndex;
	}
	
	public static void swap (int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	public static void selectionSort (int[] arr) {
		for (int i = 0; i < arr.length - 1; i++)
			swap(arr, i, minIndexFrom(arr, i + 1));
	}
	
	public static void bubbleSort (int[] arr) {
		boolean swapped = false;
		do {
			swapped = false;
			for (int i = 0; i < arr.length - 1; i++)
				if (arr[i] > arr[i + 1]) {
					swap (arr, i, i + 1);
					swapped = true;
				}
		} while (swapped);
	}
	public static void betterBubbleSort (int[] arr) {
		boolean swapped = false;
		int numSunk = 0;
		do {
			swapped = false;
			for (int i = 0; i < arr.length - 1 - numSunk; i++)
				if (arr[i] > arr[i + 1]) {
					swap (arr, i, i + 1);
					swapped = true;
				}
			if (swapped)
				numSunk++;
		} while (swapped);
	}
	
	public static void main (String[] args) {
		int arr[] = {4, 3, 2, 10, 12, 1, 5, 6};
		betterBubbleSort(arr);
		for (int item : arr)
			System.out.print(item + " ");
	}

}
