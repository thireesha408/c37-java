
public class EulerProject {
	
	public static boolean isDivisibleByAny(int num, int[] divisors) {
		for (int divisor : divisors)
			if (num % divisor == 0)
				return true;
		return false;
	}
	
	public static int sumOfMultiples(int num, int[] divisors) {
		int sum = 0;
		for (int i = 1 ; i < num ; i++)
			if (isDivisibleByAny(i, divisors)) {
				sum+= i;
			}
		return sum;
	}
	public static void main (String[] args) {
		int[] arr = {3, 5, 7, 11};
		System.out.println(sumOfMultiples(1000, arr));
	}

}
