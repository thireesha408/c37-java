
public class Day12 {
	
	public static final String[] DIGITS_IN_WORDS = 
		{"Zero", "One", "Two", "Three", "Four", 
				"Five", "Six", "Seven", "Eight", "Nine"};
	
	public static int winnings (int a, int b, int c) {
		if ((a != b) && (b != c) && (a != c))
			return 0;
		if ((a == b) && (b == c))
			return 20;
		return 10;
	}
	
	public static int max (int[] arr) {
		int max = arr[0];
		for (int item : arr)
			if (item > max)
				max = item;
		return max;
	}
	
	public static int numOccurrences (int[] arr, int value) {
		int occurrences = 0;
		for (int i = 0; i < arr.length; i++)
			if (arr[i] == value)
				occurrences++;
		return occurrences;
	}
	
	public static int maxNumEquals (int arr[]) {
		int freq[] = new int [arr.length];
		for (int i = 0; i < arr.length; i++)
			freq[i] = numOccurrences(arr, arr[i]);
		return max(freq);
	}
	
	public static String inWords (int num) {
		int[] digits = inDigits(num);
		String output = "";
		for (int i = 0; i < digits.length; i++) 
			output += DIGITS_IN_WORDS[digits[i]] + " ";
		return output;
	}
	
	public static boolean isArmstrong (int num) {
		int[] digits = inDigits(num);
		int sum = 0;
		for (int i = 0; i < digits.length; i++) {
			sum += Math.pow(digits[i], digits.length);
		}
		return sum == num;
	}
	
	public static int[] inDigits (int num) {
		int length = String.valueOf(num).length();
		int[] digits = new int[length];
		for (int i = length - 1; i >= 0; i--) {
			digits[i] = num % 10;
			num /= 10;
		}
		return digits;
	}
	
	public static void bubbleSort (int[] arr) {
		int numSwapped = -1;
		int numGuysSunk = 0;
		while (numSwapped != 0) {
			numSwapped = 0;
			for (int i = 0; i < arr.length - 1 - numGuysSunk; i++) {
				if (arr[i] > arr[i + 1]) {
					swap(arr, i, i + 1);
					numSwapped++;
				}
			}
			if (numSwapped > 0)
				numGuysSunk++;
		}
	}
	
	public static void swap (int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	
	
	public static void main (String[] args) {
		int[] arr = {12, 6, 1, 4, 3, 2, 5};
		bubbleSort(arr);
		for (int item : arr) {
			System.out.print(item + " ");
		}
		String x = "ham";
	}

}
