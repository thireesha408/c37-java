package module2;

public class Line {
	
	private double slope;
	private double intercept;
	
	public Line(double slope, double intercept) {
		this.slope = slope;
		this.intercept = intercept;
	}
	
	public Line() {
		this.slope = 1;
		this.intercept = 1;
	}
	
	public double distance(Point p) {
		//your code here
	}
	
	public Object intersection(Line other) {
		//your code here
		return null;
	}

}
