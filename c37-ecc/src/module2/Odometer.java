package module2;

public class Odometer {
	
	private long reading;
	private static final String DIGITS = "123456789";
	
	private int getLength() {
		return String.valueOf(reading).length();
	}
	
	private long max(int length) {
		return Integer.valueOf(DIGITS.substring(9 - length));
	}
	
	private long min(int length) {
		return Integer.valueOf(DIGITS.substring(0, length));
	}
	
	public Odometer (int length) {
		if (length <= 0) {
			System.out.println("invalid length, reading is going to be set to 0");
			return;
		}
		reading = min(length);
	}
	
	public Odometer(int length, long reading) {
		this.reading = reading;
		if (!isValid()) {
			reading = min(length);
		}
	}
	
	public boolean isValid() {
		long temp = reading;
		while (true) {
			if (temp % 10 <= (temp / 10) % 10)
				return false;
			if (temp < 10)
				return true;
			temp /= 10;
		}
	}
	
	public void setReading(long ourReading) {
		reading = ourReading;
		if (!isValid()) {
			System.out.println("Invalid reading set, setting to 123");
			reading = 123;
		}
	}
	
	public void incrementReading() {
		int readingLength = getLength();
		if (reading == max(readingLength)) {
			reading = min(readingLength);
			return;
		}
		reading++;
		while (!isValid())
			reading++;
	}
	
	public void decrementReading() {
		int readingLength = getLength();
		if (reading == min(readingLength)) {
			reading = max(readingLength);
			return;
		}
		reading--;
		while (!isValid())
			reading--;
	}
	
	public void incrementReadingBy(int step) {
		for (int i = 0; i < step; i++)
			incrementReading();
	}
	
	public void decrementReadingBy(int step) {
		for (int i = 0; i < step; i++)
			decrementReading();
	}
	
	public long distanceFrom (Odometer other) {
		if (other.getLength() != getLength())
			return 0;
		long distance = 0;
		Odometer temp = new Odometer(other.getLength(), other.reading);
		while(temp.reading != this.reading) {
			temp.incrementReading();
			distance++;
		}
		return distance;
	}
	
	public static long distanceBetween(Odometer o1, Odometer o2) {
		if (o1.getLength() != o2.getLength())
			return 0;
		long o1reading = o1.getReading();
		int distance = 0;
		while (o1.reading != o2.reading) {
			o1.incrementReading();
			distance++;
		}
		o1.reading = o1reading;
		return distance;
	}
	
	public String toString() {
		//your code here
		return "";
	}
	
	public long getReading () {
		return this.reading;
	}

}
