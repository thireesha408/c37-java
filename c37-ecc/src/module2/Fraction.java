package module2;

public class Fraction {
	/* As you know, Java only supports floating points 
	 * but not fractions.
	 * It might be useful for us to write a 
	 * Fraction class that stores
	 * the numerator and denominator 
	 * as private instance variables with
	 * getters and setters. 
	 * Write this Fraction class with a default constructor that 
	 * initializes it to (1/1) and a parametrized constructor that
	 * takes a numerator and a denominator. 
	 * Write a toString method as well. (4 marks)
	 * Next, write a method public void reduce() which reduces the 
	 * fraction until numerator and denominator are co-prime. (8 marks)	 
	 */

}
