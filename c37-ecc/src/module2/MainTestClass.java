package module2;

public class MainTestClass {
	
	public static void main (String[] args) {
		Cat cat = new Cat();
		System.out.println(cat.getSpecies());
		
		Human ani = new Human();
		System.out.println(ani.getSpecies());
		
		Card card = new Card(Card.ACE, Card.SPADES);
		
		Scrambler s = new Scrambler();
		System.out.println(s.scramble(cat));
		System.out.println("hello world");
		System.out.println(s.scramble(ani));
		
		R2D2 t = new R2D2();
		System.out.println(s.scramble(t));
	}

}
