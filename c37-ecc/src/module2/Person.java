package module2;

public class Person {
	
	private String name;
	private int age;
	private String gender;
	private double height;
	private double weight;
	
	public Person(String name, int age, String gender, double height, double weight) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.height = height;
		this.weight = weight;
	}
	
	public String getSleepTime() {
		return "11pm";
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", height=" + height + ", weight="
				+ weight + "]";
	}
	

}
