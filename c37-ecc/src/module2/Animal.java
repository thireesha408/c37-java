package module2;

public abstract class Animal implements Talkative{
	
	protected String species;
	protected int age;
	
	public void grow() {
		age++;
	}
	
	public String getSpecies() {
		return species;
	}

}
