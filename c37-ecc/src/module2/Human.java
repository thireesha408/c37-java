package module2;

public class Human extends Animal{
	
	public Human() {
		this.species = "Homo sapiens";
	}
	
	@Override
	public String talk() {
		return "hey!";
	}
	
	@Override
	public void grow() {
		age *= 2;
	}

}
