package module2;

public class Scrambler {
	
	public String scramble(Talkative t) {
		String alphas = "abcdefghijklmnopqrstuvwxyzabcdefghijklm";
		String toScramble = t.talk();
		String scrambled = "";
		for (int i = 0; i < toScramble.length(); i++) {
			scrambled += alphas.charAt
					(alphas.indexOf(toScramble.charAt(i)) + 13);
		}
		return scrambled;
	}

}
