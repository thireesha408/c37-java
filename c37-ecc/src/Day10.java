
public class Day10 {
	
	public static String[] fizzBuzz (int limit) {
		String[] op = new String[limit];
		for (int i = 1; i < limit; i++) {
			if (i % 5 == 0)
				op[i] = "BUZZ";
			else if (i % 15 == 0)
				op[i] = "FIZZBUZZ";
			else if (i % 3 == 0)
				op[i] = "FIZZ";
			else
				op[i] = String.valueOf(i);
				
		}
		return op;
	}
	
	public static int numOccurrences (int[] arr, int value) {
		int numOccurrences = 0;
		for (int i = 0; i < arr.length; i++)
			if (arr[i] == value)
				numOccurrences++;
		return numOccurrences;
	}
	
	public static void main(String[] args) {
		
		String[] fizzBuzzes = fizzBuzz(16);
		for (int i = 1; i < fizzBuzzes.length; i++)
			System.out.print(fizzBuzzes[i] + " ");
	}

}
